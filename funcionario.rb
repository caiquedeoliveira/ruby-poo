class Funcionario
    attr_accessor: :p_nome, :s_nome, :idade, :endereco, :matricula, :setor, :dependentes, :salario

    def initialize params
        @p_nome = params[:p_nome]
        @s_nome = params[:s_nome]
        @idade = params[:idade]
        @endereco = params[:endereco]
        @matricula = params[:matricula]
        @setor = params[:setor]
        @dependentes = params[:dependentes]
        @salario = params[:salario]
    end

    def nome_completo params
        puts "Nome completo do funcionário: #{params[:p_nome]} #{params[:s_nome]}."
    end

    def adicionar_dependente dependente 
        initialize params[:dependentes].append(dependente)
    end

    def numero_dependentes params
        puts "Número de dependentes: #{params[:dependentes].length}"
    end
end