class Departamento
    attr_accessor :funcionarios, :nome, :data
    
    def initialize params
        @funcionarios = params[:funcionarios]
        @nome = params[:nome]
        @gerente = params[:gerente]
    end
end

def numero_funcionarios params
    puts "Número de funcionários: #{params[:funcionarios].length}"
end

def adicionar_funcionario funcionario 
    initialize params[:funcionario].append(funcionario)
end

def adicionar_gerente gerente 
    initialize params[:gerente].append(gerente)
end