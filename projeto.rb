class Livro
    attr_accessor :nome, :cliente, :fundos, :departamento

    def initialize params
        @nome = params[:nome]
        @cliente = params[:cliente]
        @fundos = params[:fundos]
        @departamento = params[:departamento]
    end
end